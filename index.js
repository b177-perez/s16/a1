let number = Number(prompt("Give me a number"));

console.log("The number you provided is " + number);

/*while (number > 50){
	console.log("While: " + number);
	number--;
}*/
	
// Continue and Break Statements

for(let num = number; num > 50; num--){
	// if remainder is equal to 0, tells the code to continue to iterate
	if(num % 10 === 0){
		// console.log("The number is divisible by 10. Skipping the number: " + num);
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}
	// console.log("Continue and Break: " + num);

	if(num % 5 === 0){
		// console.log("The number is divisible by 5. Print the number: " + num);
		console.log(num);
		continue;
	}
	// console.log("Continue and Break: " + num);

	if(num <= 50){
		break;
	}

}	
console.log("The current value is at 50. Terminating the loop.");


/*let string = "supercalifragilisticexpialidocious";
let stringVowels = [ 'a', 'e', 'i', 'o', 'u',
					'A', 'E', 'I', 'O', 'U' ];
let newString = "";
     
    console.log(string); 
    for(let i = 0; i < string.length; i++)
    {
         
        if (!stringVowels.includes(string[i]))
        {
        	// console.log(string[i]); 

            newString += string[i];

            // console.log(newString); 
        }
    }
    console.log(newString);*/


let myString = "supercalifragilisticexpialidocious";
let newString = "";

console.log(myString);
for(i = 0; i < myString.length; i++){


	if(
		myString[i].toLowerCase() == "a" ||
		myString[i].toLowerCase() == "e" ||
		myString[i].toLowerCase() == "i" ||
		myString[i].toLowerCase() == "o" ||
		myString[i].toLowerCase() == "u" 
		){
		// console.log("Continue to the next iteration");
		continue;
	}

	else{

		// let myLetter = myString[i];
		// console.log(myLetter);

		newString += myString[i];

		// console.log(newString);
	}

	console.log()
	if(i > myString.length){
		break;
	}
}
console.log(newString);
